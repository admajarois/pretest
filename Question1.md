Suatu sekolah gemar mendata **nama** dan **olahraga favorit** setiap siswa yang baru masuk ke sekolah tersebut agar sekolah tersebut dapat memperoleh informasi mengenai tingkat minat siswa akan tren olahraga tertentu. Sejauh ini, hipotesis yang dikemukakan oleh Kesiswaan mengatakan bahwa Basket, Sepak Bola, dan Renang adalah olahraga yang sedang _nge-trend_, sedangkan olah raga lain tidak termasuk hitungan.

Jika data siswa disimpan menggunakan class berikut:
```
class Student {
  public String name;
  public List<Sport> sports;
}

enum Sport {
  BASKETBALL,
  SOCCER, 
  SWIMMING, 
  E_SPORT, 
  CHESS,
  OTHER
}
```

Lengkapilah fungsi-fungsi berikut ini:
```
List<Student> getStudentsWhoLoveBasketballAndSoccer(List<Student> students) {
  ...
}

List<Student> getStudentsWhoLoveBasketballOrSwimming(List<Student> students) {
  ...
}

List<Student> getStudentsWhoOnlyLoveSoccer(List<Student> students) {
  ...
}
```